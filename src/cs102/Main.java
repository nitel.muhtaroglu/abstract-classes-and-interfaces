package cs102;

public class Main {

    public static void main(String[] args) {
        Shape circularShape = new Circle(0, 0, 10);
        System.out.println(circularShape instanceof Circle);
        System.out.println(circularShape instanceof Shape);
        System.out.println(circularShape instanceof Planar);
    }
}
