package cs102;

public class Rectangle {
    private float width;
    private float height;

    public Rectangle(int x, int y, float width, float height) {
        this.width = width;
        this.height = height;
    }

    public float getArea() {
        return this.width * this.height;
    }
}
