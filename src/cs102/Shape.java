package cs102;

public abstract class Shape {
    public abstract float getPerimeter();
}
