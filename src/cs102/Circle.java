package cs102;

public class Circle extends Shape implements Planar {
    private int x;
    private int y;
    private float radius;

    public Circle(int x, int y, float radius) {
        this.x = x;
        this.y = y;
        this.radius = radius;
    }

    public float getArea() {
        return this.radius * this.radius * 3.14f;
    }

    public float getPerimeter() {
        return 2 * 3.14f * this.radius;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }
}
